package com.amd.telecom.assesment.exception;

public class OpenWeatherDataException extends Exception {
    public OpenWeatherDataException(String message) {
        super(message);
    }
}
