package com.amd.telecom.assesment.converter;

import com.amd.telecom.assesment.model.WeatherData;
import com.amd.telecom.assesment.provider.OpenWeatherProvider;
import com.amd.telecom.assesment.type.WeatherConverter;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * If pure java wasn't a requirement i would have used a JSON library such as org.json
 */
public class OpenWeatherConverter implements WeatherConverter {

    Logger logger = Logger.getLogger(this.getClass().getName());

    private static volatile OpenWeatherConverter openWeatherConverter;

    private OpenWeatherConverter() {}

    public static OpenWeatherConverter getInstance() {
        if (openWeatherConverter == null) {
            synchronized (OpenWeatherProvider.class) {
                if (openWeatherConverter == null) {
                    openWeatherConverter = new OpenWeatherConverter();
                }
            }
        }
        return openWeatherConverter;
    }

    @Override
    public Optional<WeatherData> convert(String response) {

        try {
            String firstDelimiter = "\"temp\":";
            String secondDelimiter = ",\"feels_like\":";

            int tempIndex = response.lastIndexOf(firstDelimiter) + firstDelimiter.length();
            int feelsLikeIndex = response.indexOf(secondDelimiter);
            String temperature = response.substring(tempIndex, feelsLikeIndex);

            double temp = Double.parseDouble(temperature);
            return Optional.of(new WeatherData(temp));
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }

        return Optional.empty();
    }

}
