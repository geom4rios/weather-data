package com.amd.telecom.assesment.smalExercises;

import java.util.Arrays;
import java.util.Optional;

/*
1. Create a function (findSeven) that takes an array of numbers and return "Found"

if the character 7 appears in the array of the numbers. Otherwise, return "there is no 7 in the array".

Examples :

findSeven([1, 2, 3, 4, 5, 6, 7]) ➞ "Found"

// 7 contains the number seven.



findSeven([8, 6, 33, 100]) ➞ "there is no 7 in the array"

// None of the items contain 7 within them.



findSeven([2, 55, 60, 97, 86]) ➞ "Found"

// 97 contains the number seven.
 */
public class FindSeven {

    public static void main(String[] args) {

        int[][] testArrays = new int[][] {
                { 1, 2, 3, 4, 5, 6, 7 },
                { 8, 6, 33, 100 },
                { 2, 55, 60, 97, 86 }
        };

        for (int[] testArray : testArrays) {
            String result = findSeven(testArray);
            System.out.println(result);
        }
    }

    private static String findSeven(int[] array) {

        Optional<String> found = Arrays.stream(array)
                .mapToObj(String::valueOf)
                .filter(s -> s.contains("7"))
                .findAny();

        if (found.isPresent()) {
            return "Found";
        } else {
            return "There is no 7 in the array";
        }
    }

}
