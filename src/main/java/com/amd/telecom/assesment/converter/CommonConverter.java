package com.amd.telecom.assesment.converter;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class CommonConverter {

    Logger logger = Logger.getLogger(CommonConverter.class.getName());

    Optional<String> readValueFromJsonBetweenTwoKeys(String jsonString, String firstKey, String secondKey) {
        try {
            firstKey = "\"" + firstKey + "\":";
            secondKey = ",\"" + secondKey + "\":";
            int tempIndex = jsonString.lastIndexOf(firstKey) + firstKey.length();
            int feelsLikeIndex = jsonString.indexOf(secondKey);
            String valueWithDoubleQuotes = jsonString.substring(tempIndex, feelsLikeIndex);
            String toReturn = valueWithDoubleQuotes.replace("\"", "");
            return Optional.of(toReturn);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return Optional.empty();
    }

}
