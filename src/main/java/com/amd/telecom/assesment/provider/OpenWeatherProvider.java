package com.amd.telecom.assesment.provider;

import com.amd.telecom.assesment.client.SharedHttpClient;
import com.amd.telecom.assesment.type.WeatherProvider;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenWeatherProvider extends CommonProvider implements WeatherProvider {

    Logger logger = Logger.getLogger(this.getClass().getName());
    private Map<String, String> OPEN_WEATHER_CONFIGURATION = new HashMap<>();
    private static volatile OpenWeatherProvider openWeatherProvider;
    private final SharedHttpClient sharedHttpClient;
    private String API_KEY = "";

    private OpenWeatherProvider() {
        this.sharedHttpClient = SharedHttpClient.getInstance();
        init();
    }

    public static OpenWeatherProvider getInstance() {
        if (openWeatherProvider == null) {
            synchronized (OpenWeatherProvider.class) {
                if (openWeatherProvider == null) {
                    openWeatherProvider = new OpenWeatherProvider();
                }
            }
        }
        return openWeatherProvider;
    }

    private void init() {
        loadConfiguration(OPEN_WEATHER_CONFIGURATION, "open-weather-configuration", "OPEN WEATHER CONFIGURATION");
        this.API_KEY = readKeyFromConfigMap(OPEN_WEATHER_CONFIGURATION, "apiKey", "Open Weather");
    }

    @Override
    public Optional<String> fetchWeatherDataForCity(String city) {

        String url = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=metric", city, API_KEY);
        HttpRequest httpRequest = buildRequest(url);

        try {
            HttpResponse<String> httpResponse = sharedHttpClient.send(httpRequest);
            return Optional.of(httpResponse.body());
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            Thread.currentThread().interrupt();
        }

        return Optional.empty();
    }

    private HttpRequest buildRequest(String url) {

        HttpRequest.Builder builder = HttpRequest.newBuilder();

        // set url
        builder.uri(URI.create(url));

        return builder.build();
    }
}
