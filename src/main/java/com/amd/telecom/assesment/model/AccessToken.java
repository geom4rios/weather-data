package com.amd.telecom.assesment.model;

import java.time.LocalDateTime;

public class AccessToken {

    private String value;

    private LocalDateTime expiresIn;

    public AccessToken(String value, LocalDateTime expiresIn) {
        this.value = value;
        this.expiresIn = expiresIn;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LocalDateTime getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(LocalDateTime expiresIn) {
        this.expiresIn = expiresIn;
    }

    @Override
    public String toString() {
        return "AccessToken{ \n" +
                "value='" + value + '\'' + ',' + '\n' +
                "expiresIn='" + expiresIn + '\'' + ',' + '\n' +
                '}';
    }
}
