package com.amd.telecom.assesment.utils;

import com.amd.telecom.assesment.model.AccessToken;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

class FileReaderWriterTest {

    public static void main(String[] args) throws IOException {
        FileReaderWriterTest fileReaderWriterTest = new FileReaderWriterTest();
        fileReaderWriterTest.writeFile();
        fileReaderWriterTest.readFile();
    }

    void readFile() throws IOException {
        String userDir = System.getProperty("user.dir") + "\\access_token.json";
        List<String> strings = FileReaderWriter.readFile(new File(userDir));
        System.out.println(strings);
        for (String str : strings) {
            System.out.println(str);
        }
    }

    void writeFile() throws IOException {
        AccessToken accessToken = new AccessToken("1d754fec-68e9-4704-a284-784c348f425c", LocalDateTime.now());
        String userDir = System.getProperty("user.dir") + "\\access_token.json";

        String accessTokenStr = accessToken.toString();
        String[] split = accessTokenStr.split("\n");
        List<String> strings = Arrays.asList(split);

        FileReaderWriter.writeFile(strings, new File(userDir));
    }

}