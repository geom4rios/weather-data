package com.amd.telecom.assesment.type;

import com.amd.telecom.assesment.model.WeatherData;

import java.util.Optional;

public interface WeatherConverter {

    Optional<WeatherData> convert(String response);

}
