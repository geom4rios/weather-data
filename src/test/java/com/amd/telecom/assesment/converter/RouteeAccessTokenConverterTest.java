package com.amd.telecom.assesment.converter;

import com.amd.telecom.assesment.exception.AccessTokenConversionException;
import com.amd.telecom.assesment.model.AccessToken;
import org.junit.jupiter.api.Test;

class RouteeAccessTokenConverterTest {

    @Test
    public void convert() throws AccessTokenConversionException {
        RouteeAccessTokenConverter routeeAccessTokenConverter = RouteeAccessTokenConverter.getInstance();
        AccessToken accessToken = routeeAccessTokenConverter.convert(sampleResponse)
                .orElseThrow(() -> new AccessTokenConversionException("Unable to convert from sample response"));

        if (!accessToken.getValue().equals("573b65a5-3499-440e-8fa6-c5e67a2c4edd")) {
            throw new AccessTokenConversionException("Unexpected value for access_token! Got value: " + accessToken.getValue());
        }
        /*if (!accessToken.getExpiresIn().equals("3599")) {
            throw new AccessTokenConversionException("Unexpected value for expires_in! Got value: " + accessToken.getExpiresIn());
        }*/

        System.out.println(accessToken);
        System.out.println("TEST PASSED!");
    }

    String sampleResponse = "{\"access_token\":\"573b65a5-3499-440e-8fa6-c5e67a2c4edd\",\"token_type\":\"bearer\",\"expires_in\":3599,\"scope\":\"voice lookup virtual_number contact report sms 2step number_validator account failover number_pool forms transactional_email email_sender promotional_email email_validator url_analyzer application_security\",\"permissions\":[\"MT_ROLE_LOOKUP\",\"MT_ROLE_NUMBER_VALIDATOR\",\"MT_ROLE_ACCOUNT_FINANCE\",\"MT_ROLE_SMS\",\"MT_ROLE_REPORT\",\"MT_ROLE_VOICE\",\"MT_ROLE_NUMBER_POOL\",\"MT_ROLE_2STEP\",\"MT_ROLE_VIRTUAL_NUMBER\",\"MT_ROLE_CONTACT\",\"MT_ROLE_FAILOVER\",\"MT_ROLE_FORMS\",\"MT_ROLE_TRANSACTIONAL_EMAIL\",\"MT_ROLE_EMAIL_SENDER\",\"MT_ROLE_PRICING_PACKAGES\",\"MT_ROLE_PROMOTIONAL_EMAIL\",\"MT_ROLE_EMAIL_VALIDATOR\",\"MT_ROLE_URL_ANALYZER\",\"MT_ROLE_APPLICATION_SECURITY\"]}";
}