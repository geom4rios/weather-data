package com.amd.telecom.assesment.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64EncoderDecoder {

    private Base64EncoderDecoder() {}

    public static String encode(String value) {
        return Base64.getUrlEncoder().encodeToString(value.getBytes(StandardCharsets.UTF_8));
    }

    public static byte[] decode(String value) {
        return Base64.getUrlDecoder().decode(value);
    }

}
