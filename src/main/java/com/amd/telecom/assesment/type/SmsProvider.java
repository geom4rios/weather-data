package com.amd.telecom.assesment.type;

import java.util.Optional;

public interface SmsProvider {

    Optional<String> send(String message, String phoneNumber, String accessToken);

}
