## Weather APP

This is a simple weather app that reads the Thessaloniki weather and sends an sms message using the routee API. 

<b>The application is written in pure java and no external libraries or frameworks were used.</b>

## How to build the project using maven

`mvn clean package -DskipTests`

## How to run the weather app

After building the project using maven then from the project's root directory run below command

`java -cp .\target\amd-telecom-1.0-SNAPSHOT.jar com.amd.telecom.assesment.WeatherApp`

## How to run the small exercises

After you compile the project, run the below commands from the root directory.

#### Exercise 1 (Found)

`java -cp ./target/classes com.amd.telecom.assesment.smalExercises.FindSeven`

#### Exercise 2 (Digit Sum)

`java -cp ./target/classes com.amd.telecom.assesment.smalExercises.DigitSum`

#### Exercise 3 (doRemake)

`java -cp ./target/classes com.amd.telecom.assesment.smalExercises.DoRemake`

