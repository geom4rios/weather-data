package com.amd.telecom.assesment.type;

import com.amd.telecom.assesment.model.AccessToken;

import java.util.Optional;

public interface AccessTokenConverter {

    Optional<AccessToken> convert(String response);

}
