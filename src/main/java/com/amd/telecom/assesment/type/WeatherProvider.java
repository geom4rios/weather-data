package com.amd.telecom.assesment.type;

import java.util.Optional;

public interface WeatherProvider {

    Optional<String> fetchWeatherDataForCity(String city);

}
