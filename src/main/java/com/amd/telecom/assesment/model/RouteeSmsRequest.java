package com.amd.telecom.assesment.model;

public class RouteeSmsRequest {

    private String body;

    private String to;

    private String from;

    public RouteeSmsRequest(String body, String to) {
        this.body = body;
        this.to = to;
        this.from = "amdTelecom";
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "{" +
                "\"body\": \"" + body + '\"' +
                ",\"to\": \"" + to + '\"' +
                ",\"from\": \"" + from + '\"' +
                '}';
    }
}
