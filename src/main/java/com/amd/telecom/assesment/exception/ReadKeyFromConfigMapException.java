package com.amd.telecom.assesment.exception;

public class ReadKeyFromConfigMapException extends RuntimeException {
    public ReadKeyFromConfigMapException(String message) {
        super(message);
    }
}
