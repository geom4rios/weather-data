package com.amd.telecom.assesment.provider;

import com.amd.telecom.assesment.client.SharedHttpClient;
import com.amd.telecom.assesment.model.RouteeSmsRequest;
import com.amd.telecom.assesment.type.SmsProvider;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RouteeSmsProvider extends CommonProvider implements SmsProvider {

    Logger logger = Logger.getLogger(this.getClass().getName());
    private static volatile RouteeSmsProvider routeeSmsProvider;
    private final SharedHttpClient sharedHttpClient;

    public static RouteeSmsProvider getInstance() {
        if (routeeSmsProvider == null) {
            synchronized (RouteeSmsProvider.class) {
                if (routeeSmsProvider == null) {
                    routeeSmsProvider = new RouteeSmsProvider();
                }
            }
        }
        return routeeSmsProvider;
    }

    private RouteeSmsProvider() {
        this.sharedHttpClient = SharedHttpClient.getInstance();
    }

    private HttpRequest buildRequest(String message, String phoneNumber, String accessToken) {

        HttpRequest.Builder builder = HttpRequest.newBuilder();

        String url = "https://connect.routee.net/sms";

        // set url
        builder.uri(URI.create(url))
                .POST(bodyPublisher(message, phoneNumber));

        Map<String, String> headers = headers(accessToken);
        // add headers
        headers.keySet().forEach((key) -> builder.header(key, headers.get(key)));

        return builder.build();
    }

    private HttpRequest.BodyPublisher bodyPublisher(String message, String phoneNumber) {
        RouteeSmsRequest routeeSmsRequest = new RouteeSmsRequest(message, phoneNumber);
        return HttpRequest.BodyPublishers.ofString(routeeSmsRequest.toString(), StandardCharsets.UTF_8);
    }

    private Map<String, String> headers(String accessToken) {
        Map<String, String> routeeSmsHeaders = new HashMap<>();
        routeeSmsHeaders.put("authorization", "Bearer " + accessToken);
        routeeSmsHeaders.put("Content-Type", "application/json");
        return routeeSmsHeaders;
    }

    @Override
    public Optional<String> send(String message, String phoneNumber, String accessToken) {
        HttpRequest httpRequest = buildRequest(message, phoneNumber, accessToken);
        try {
            HttpResponse<String> response = sharedHttpClient.send(httpRequest);
            return Optional.of(response.body());
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        } catch (InterruptedException e) {
            logger.log(Level.WARNING, "Thread got an interrupted exception");
            logger.log(Level.SEVERE, e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
        return Optional.empty();
    }

}
