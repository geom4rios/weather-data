package com.amd.telecom.assesment.converter;

import com.amd.telecom.assesment.exception.OpenWeatherDataConversionException;
import com.amd.telecom.assesment.model.WeatherData;
import org.junit.jupiter.api.Test;

class OpenWeatherConverterTest {

    @Test
    public void convert() throws OpenWeatherDataConversionException {
        OpenWeatherConverter openWeatherConverter = OpenWeatherConverter.getInstance();
        WeatherData weatherData = openWeatherConverter.convert(sampleResponse)
                .orElseThrow(() -> new OpenWeatherDataConversionException("Unable to convert sample response!"));
        if (weatherData.getTemperature() == 10.85) {
            System.out.println(weatherData);
            System.out.println("TEST PASSED!");
        } else {
            throw new OpenWeatherDataConversionException("Unexpected value!");
        }
    }

    String sampleResponse = "{\"coord\":{\"lon\":22.9439,\"lat\":40.6403},\"weather\":[{\"id\":701,\"main\":\"Mist\",\"description\":\"mist\",\"icon\":\"50n\"}],\"base\":\"stations\",\"main\":{\"temp\":10.85,\"feels_like\":10.31,\"temp_min\":9.64,\"temp_max\":12.75,\"pressure\":1013,\"humidity\":89},\"visibility\":6000,\"wind\":{\"speed\":0,\"deg\":0},\"clouds\":{\"all\":40},\"dt\":1640712529,\"sys\":{\"type\":2,\"id\":2036703,\"country\":\"GR\",\"sunrise\":1640670674,\"sunset\":1640704086},\"timezone\":7200,\"id\":734077,\"name\":\"Thessaloniki\",\"cod\":200}";
}