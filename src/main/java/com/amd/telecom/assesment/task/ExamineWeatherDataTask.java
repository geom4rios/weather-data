package com.amd.telecom.assesment.task;

import com.amd.telecom.assesment.converter.OpenWeatherConverter;
import com.amd.telecom.assesment.converter.RouteeAccessTokenConverter;
import com.amd.telecom.assesment.exception.AccessTokenConversionException;
import com.amd.telecom.assesment.exception.AccessTokenGenerateException;
import com.amd.telecom.assesment.exception.OpenWeatherDataConversionException;
import com.amd.telecom.assesment.exception.OpenWeatherDataException;
import com.amd.telecom.assesment.model.AccessToken;
import com.amd.telecom.assesment.model.WeatherData;
import com.amd.telecom.assesment.provider.OpenWeatherProvider;
import com.amd.telecom.assesment.provider.RouteeAccessTokenPovider;
import com.amd.telecom.assesment.provider.RouteeSmsProvider;
import com.amd.telecom.assesment.type.WeatherConverter;
import com.amd.telecom.assesment.type.WeatherProvider;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExamineWeatherDataTask extends TimerTask {

    Logger logger = Logger.getLogger(this.getClass().getName());
    private final AtomicInteger counter = new AtomicInteger(0);
    private final Timer timer;

    public ExamineWeatherDataTask(Timer timer) {
        this.timer = timer;
    }

    @Override
    public void run() {
        if (counter.get() > 9) {
            this.timer.cancel();
            this.cancel();
        } else {
            logger.log(Level.INFO, "EXECUTION TIME: {0}", LocalDateTime.now());
            examineWeather();
            int executionNumber = counter.getAndIncrement() + 1;
            logger.log(Level.INFO,"EXECUTION NUMBER: {0}", executionNumber);
        }
    }

    private void examineWeather() {
        try {
            // fetch data from open weather api
            WeatherProvider openWeatherProvider = OpenWeatherProvider.getInstance();
            String openWeatherResponse = openWeatherProvider.fetchWeatherDataForCity("Thessaloniki")
                    .orElseThrow(() -> new OpenWeatherDataException("Unable to fetch data from Open Weather API"));
            logger.log(Level.INFO, "Open weather response: {0}", openWeatherResponse);

            // convert data from open weather api to local entity
            WeatherConverter openWeatherConverter = OpenWeatherConverter.getInstance();
            WeatherData weatherData = openWeatherConverter.convert(openWeatherResponse)
                    .orElseThrow(() -> new OpenWeatherDataConversionException("Unable to convert open weather data to local entity: " + WeatherData.class.getName()));
            logger.log(Level.INFO,"Weather data: {0}", weatherData);

            // send the sms based on weather data
            sendSms(weatherData);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void sendSms(WeatherData weatherData) {
        String myName = "Marios Georgiou";
        String phoneNumber = "+306922222222";

        String accessToken = getAccessToken()
                .orElseThrow(() -> new RuntimeException("Unable to get access token!"));

        RouteeSmsProvider routeeSmsProvider = RouteeSmsProvider.getInstance();
        if (weatherData.getTemperature() > 20) {
            logger.info("Temperature more than 20C.");
            String moreThan20Msg = String.format("%s, temperature more than 20C, %sC.", myName, weatherData.getTemperature());
            routeeSmsProvider.send(moreThan20Msg, phoneNumber, accessToken).ifPresentOrElse(
                    response -> logger.log(Level.INFO,"Got response from sms provider: {0}", response),
                    () -> logger.info("Failed to get response from sms provider!")
            );
        } else {
            logger.info("Temperature less than 20C.");
            String lessThan20Msg = String.format("%s, temperature less than 20C, %sC.", myName, weatherData.getTemperature());
            routeeSmsProvider.send(lessThan20Msg, phoneNumber, accessToken).ifPresentOrElse(
                    response -> logger.log(Level.INFO, "Got response from sms provider: {0}", response),
                    () -> logger.info("Failed to get response from sms provider!")
            );
        }

    }

    private Optional<String> getAccessToken() {

        RouteeAccessTokenPovider routeeAccessTokenPovider = RouteeAccessTokenPovider.getInstance();
        if (routeeAccessTokenPovider.isAccessTokenValid()) {
            logger.info("Access token is valid!");
            return Optional.of(routeeAccessTokenPovider.getAccessToken().getValue());
        } else {
            logger.warning("Access token is invalid!");
            logger.info("Generating new access token!");
            try {
                String accessTokenResponse = routeeAccessTokenPovider.generateAccessToken()
                        .orElseThrow(() -> new AccessTokenGenerateException("Unable to generate access_token for routee"));
                logger.info("Got access token response: " + accessTokenResponse);

                RouteeAccessTokenConverter routeeAccessTokenConverter = RouteeAccessTokenConverter.getInstance();
                AccessToken accessToken = routeeAccessTokenConverter.convert(accessTokenResponse)
                        .orElseThrow(() -> new AccessTokenConversionException("Unable to convert access token to local entity for routee, accessTokenResponse: " + accessTokenResponse));
                logger.info("Converting access token, AccessToken: " + accessToken);
                routeeAccessTokenPovider.writeAccessTokenToFile(accessToken);

                return Optional.of(accessToken.getValue());
            } catch (AccessTokenGenerateException | AccessTokenConversionException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return Optional.empty();
    }

}
