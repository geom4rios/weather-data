package com.amd.telecom.assesment.converter;

import com.amd.telecom.assesment.exception.AccessTokenConversionException;
import com.amd.telecom.assesment.model.AccessToken;
import com.amd.telecom.assesment.type.AccessTokenConverter;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RouteeAccessTokenConverter extends CommonConverter implements AccessTokenConverter {

    Logger logger = Logger.getLogger(this.getClass().getName());
    private static volatile RouteeAccessTokenConverter routeeAccessTokenConverter;

    private RouteeAccessTokenConverter() {}

    public static RouteeAccessTokenConverter getInstance() {
        if (routeeAccessTokenConverter == null) {
            synchronized (RouteeAccessTokenConverter.class) {
                if (routeeAccessTokenConverter == null) {
                    routeeAccessTokenConverter = new RouteeAccessTokenConverter();
                }
            }
        }
        return routeeAccessTokenConverter;
    }

    @Override
    public Optional<AccessToken> convert(String response) {

        try {
            String accessToken = getAccessTokenValue(response);
            String expiresIn = getExpiresInValue(response);

            LocalDateTime expiresInDateTime = LocalDateTime.now()
                    .plusSeconds(Long.parseLong(expiresIn) - 5L);

            return Optional.of(new AccessToken(accessToken, expiresInDateTime));
        } catch (AccessTokenConversionException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return Optional.empty();
    }

    private String getAccessTokenValue(String response) throws AccessTokenConversionException {
        String firstKey = "access_token";
        String secondKey = "token_type";

        return readValueFromJsonBetweenTwoKeys(response, firstKey, secondKey)
                .orElseThrow(() -> new AccessTokenConversionException("Unable to read access_token value correctly from json response: " + response));
    }

    private String getExpiresInValue(String response) throws AccessTokenConversionException {

        String firstKey = "expires_in";
        String secondKey = "scope";

        return readValueFromJsonBetweenTwoKeys(response, firstKey, secondKey)
                .orElseThrow(() -> new AccessTokenConversionException("Unable to read expires_in value correctly from json response: " + response));

    }

}
