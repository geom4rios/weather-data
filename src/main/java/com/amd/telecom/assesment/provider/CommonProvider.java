package com.amd.telecom.assesment.provider;

import com.amd.telecom.assesment.exception.ReadKeyFromConfigMapException;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Logger;

public abstract class CommonProvider {

    Logger logger = Logger.getLogger(this.getClass().getName());

    protected void loadConfiguration(Map<String, String> map, String fileName, String message) {
        ResourceBundle rb = ResourceBundle.getBundle(fileName);
        Set<String> keys = rb.keySet();
        keys.forEach(key -> map.put(key, rb.getString(key)));
        logger.info("######### " + message + " #########");
        logger.info(map.toString());
        logger.info("###################################");
    }

    protected String readKeyFromConfigMap(Map<String, String> map, String keyName, String appName) {
        if (map.containsKey(keyName)) {
            String apiKeyFromConfiguration = map.get(keyName);
            if (apiKeyFromConfiguration.isEmpty() || apiKeyFromConfiguration.isBlank()) {
                String errMsg = String.format("Key %s for %s is empty or blank", keyName, appName);
                throw new ReadKeyFromConfigMapException(errMsg);
            } else {
                return apiKeyFromConfiguration;
            }
        } else {
            String errMsg = String.format("No key %s provided for %s", keyName, appName);
            throw new ReadKeyFromConfigMapException(errMsg);
        }
    }

}
