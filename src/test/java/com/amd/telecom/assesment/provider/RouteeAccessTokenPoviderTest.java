package com.amd.telecom.assesment.provider;

import org.junit.jupiter.api.Test;

class RouteeAccessTokenPoviderTest {

    @Test
    public void readAccessTokenFromFile() {
        RouteeAccessTokenPovider routeeAccessTokenPovider = RouteeAccessTokenPovider.getInstance();
        routeeAccessTokenPovider.readAccessTokenFromFile().ifPresentOrElse(
                (t) -> {
                    System.out.println("TEST PASSED!");
                    System.out.println("Access token: " + t);
                },
                () -> {
                    System.out.println("TEST FAILED!");
                }
        );

    }

}