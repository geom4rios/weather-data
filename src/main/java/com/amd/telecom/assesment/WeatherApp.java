package com.amd.telecom.assesment;

import com.amd.telecom.assesment.task.ExamineWeatherDataTask;

import java.util.Timer;
import java.util.TimerTask;

public class WeatherApp {

    public static void main(String[] args) {

        Timer timer = new Timer();
        TimerTask examineWeatherDataTask = new ExamineWeatherDataTask(timer);

        // 600000 = 10 minutes
        long periodInMillis = 600000L;
        timer.scheduleAtFixedRate(examineWeatherDataTask, 0L, periodInMillis);

    }

}
