package com.amd.telecom.assesment.model;

public class WeatherData {

    private double temperature;

    public WeatherData(double temperature) {
        this.temperature = temperature;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "temperature=" + temperature +
                '}';
    }
}
