package com.amd.telecom.assesment.exception;

public class AccessTokenGenerateException extends Exception {
    public AccessTokenGenerateException(String message) {
        super(message);
    }
}
