package com.amd.telecom.assesment.provider;

import com.amd.telecom.assesment.client.SharedHttpClient;
import com.amd.telecom.assesment.model.AccessToken;
import com.amd.telecom.assesment.type.AccessTokenPovider;
import com.amd.telecom.assesment.utils.Base64EncoderDecoder;
import com.amd.telecom.assesment.utils.FileReaderWriter;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class RouteeAccessTokenPovider extends CommonProvider implements AccessTokenPovider {

    Logger logger = Logger.getLogger(this.getClass().getName());
    private static volatile RouteeAccessTokenPovider routeeAccessTokenPovider;

    Map<String, String> ROUTEE_API_CONFIGURATION = new HashMap<>();
    private String ROUTEE_APPLICATION_ID = "";
    private String ROUTEE_APPLICATION_SECRET = "";
    private final SharedHttpClient sharedHttpClient;
    private AccessToken accessToken;
    private final static String ACCESS_TOKEN_FILE = System.getProperty("user.dir") + "\\access_token.json";

    public static RouteeAccessTokenPovider getInstance() {
        if (routeeAccessTokenPovider == null) {
            synchronized (RouteeAccessTokenPovider.class) {
                if (routeeAccessTokenPovider == null) {
                    routeeAccessTokenPovider = new RouteeAccessTokenPovider();
                }
            }
        }
        return routeeAccessTokenPovider;
    }

    private RouteeAccessTokenPovider() {
        this.sharedHttpClient = SharedHttpClient.getInstance();
        init();
    }

    private void init() {
        loadConfiguration(ROUTEE_API_CONFIGURATION, "routee-api-configuration", "ROUTEE CONFIGURATION");
        this.ROUTEE_APPLICATION_ID = readKeyFromConfigMap(ROUTEE_API_CONFIGURATION, "appId", "Routee API");
        this.ROUTEE_APPLICATION_SECRET = readKeyFromConfigMap(ROUTEE_API_CONFIGURATION, "appSecret", "Routee API");
        readAccessTokenFromFile().ifPresentOrElse(
                aToken -> this.accessToken = aToken,
                () -> logger.info("Unable to read access token from file, file might not exist or something went wrong when trying to read!")
        );
    }

    private HttpRequest buildRequest(String base64EncodedCredentials) {

        HttpRequest.Builder builder = HttpRequest.newBuilder();

        String url = "https://auth.routee.net/oauth/token";

        // set url
        builder.uri(URI.create(url))
                .POST(bodyPublisher());

        Map<String, String> headers = headers(base64EncodedCredentials);
        // add headers
        headers.keySet().forEach((key) -> builder.header(key, headers.get(key)));

        return builder.build();
    }

    private HttpRequest.BodyPublisher bodyPublisher() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("grant_type", "client_credentials");
        String form = parameters.keySet().stream()
                .map(key -> key + "=" + URLEncoder.encode(parameters.get(key), StandardCharsets.UTF_8))
                .collect(Collectors.joining("&"));
        return HttpRequest.BodyPublishers.ofString(form, StandardCharsets.UTF_8);
    }

    private Map<String, String> headers(String base64EncodedCredentials) {
        Map<String, String> routeeSmsHeaders = new HashMap<>();
        routeeSmsHeaders.put("authorization", "Basic " + base64EncodedCredentials);
        routeeSmsHeaders.put("Content-Type", "application/x-www-form-urlencoded");
        return routeeSmsHeaders;
    }

    public boolean isAccessTokenValid() {
        if (this.accessToken == null) {
            return false;
        }
        return !this.accessToken.getExpiresIn().isBefore(LocalDateTime.now());
    }

    public AccessToken getAccessToken() {
        return this.accessToken;
    }

    @Override
    public Optional<String> generateAccessToken() {

        try {
            String toEncode = String.format("%s:%s", ROUTEE_APPLICATION_ID, ROUTEE_APPLICATION_SECRET);
            String base64encodedCredentials = Base64EncoderDecoder.encode(toEncode);

            HttpRequest httpRequest = buildRequest(base64encodedCredentials);
            HttpResponse<String> httpResponse = sharedHttpClient.send(httpRequest);

            int responseStatusCode = httpResponse.statusCode();
            if (responseStatusCode == 200) {
                return Optional.of(httpResponse.body());
            } else {
                logger.log(Level.SEVERE, "ERROR RESPONSE WHEN TRYING TO GENERATE ACCESS TOKEN: " + httpResponse.body());
                return Optional.empty();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        } catch (InterruptedException e) {
            logger.log(Level.INFO, "Thread got an interrupted exception");
            logger.log(Level.SEVERE, e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
        return Optional.empty();
    }

    public Optional<AccessToken> readAccessTokenFromFile() {
        try {
            if ( Files.exists(Paths.get(ACCESS_TOKEN_FILE)) ) {
                List<String> accessTokenFileLines = FileReaderWriter.readFile(new File(ACCESS_TOKEN_FILE));
                return convertFileContentsToAccessToken(accessTokenFileLines);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Something went wrong when trying to read access token file from filesystem.");
            logger.log(Level.SEVERE, "Path to file: " + ACCESS_TOKEN_FILE);
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return Optional.empty();
    }

    private Optional<AccessToken> convertFileContentsToAccessToken(List<String> accessTokenFileLines) throws IOException {
        String accessTokenValue = null;
        LocalDateTime expiresIn = null;
        for (String line : accessTokenFileLines) {
            if (line.contains("value=")) {
                accessTokenValue = readValueFromLineByDelimiters(line, "value=", ",");
            } else if (line.contains("expiresIn=")) {
                String expiresInStrValue = readValueFromLineByDelimiters(line, "expiresIn=", ",");
                expiresIn = LocalDateTime.parse(expiresInStrValue);
            }
        }

        if (accessTokenValue == null) {
            throw new IOException("Unable to read access token from read file correctly!");
        }
        if (expiresIn == null) {
            throw new IOException("Unable to read expiresIn value correctly");
        }
        return Optional.of(new AccessToken(accessTokenValue, expiresIn));
    }

    private String readValueFromLineByDelimiters(String line, String d1, String d2) {
        String value = line.substring(
                line.indexOf(d1) + d1.length(),
                line.indexOf(d2)
        );
        return value.replace("'", "");
    }

    public void writeAccessTokenToFile(AccessToken accessToken) {
        try {
            FileReaderWriter.writeFile(Arrays.asList(accessToken.toString().split("\n")), new File(ACCESS_TOKEN_FILE));
            logger.log(Level.INFO, "Access token written to file successfully!");
        } catch (Exception e) {
            logger.log(Level.WARNING, "UNABLE TO WRITE ACCESS TOKEN TO FILE");
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }
}
