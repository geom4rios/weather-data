package com.amd.telecom.assesment.smalExercises;

import java.util.Arrays;
import java.util.stream.Collectors;

/*
3.Create a function (doRemake) that takes a string of words and

- Move the first letter of each word to the end of the word.

- Add "ay" to the end of the word.

- Words starting with a vowel (a,e,i,o,u, A, E, I, O, U) simply have "way" appended to the end.


Be sure to preserve proper capitalization and punctuation.



Examples


doRemake("Cats are great pets.")

returns "Atscay areway reatgay etspay."


doRemake("Tom got a small piece of pie.")

returns

"Omtay otgay away mallsay iecepay ofway iepay."


doRemake("He told us a very exciting tale.")



returns "Ehay oldtay usway away eryvay excitingway aletay."


 */
public class DoRemake {

    public static void main(String[] args) {

        String[] testValues = new String[] {
                "Cats are great pets.",
                "Tom got a small piece of pie.",
                "He told us a very exciting tale."
        };

        for (String testValue : testValues) {
            String result = doRemake(testValue);
            System.out.println(result);
        }
    }

    private static String doRemake(String value) {
        String[] values = value.split(" "); // breaks string into array by given delimiter

        return Arrays.stream(values)
                .map(DoRemake::remake)
                .collect(Collectors.joining(" "));
    }

    private static String remake(String value) {
        if (startsWithVowel(value)) {
            return movePunctuationToEnd(value + "way");
        } else {
            String firstToEnd = moveFirstToEnd(value) + "ay";
            return movePunctuationToEnd(firstToEnd);
        }
    }

    private static String movePunctuationToEnd(String value) {
        if (value.contains(",") ) {
            return value.replace(",", "") + ",";
        } else if (value.contains(".")) {
            return value.replace(".", "") + ".";
        } else {
            return value;
        }
    }

     // a,e,i,o,u, A, E, I, O, U
    private static boolean startsWithVowel(String value) {
        return value.startsWith("a") ||
                value.startsWith("A") ||
                value.startsWith("e") ||
                value.startsWith("E") ||
                value.startsWith("i") ||
                value.startsWith("I") ||
                value.startsWith("o") ||
                value.startsWith("O") ||
                value.startsWith("u") ||
                value.startsWith("U");
    }

    private static String moveFirstToEnd(String value) {
        char firstLetter = value.charAt(0);
        String stringMissingFirst = value.substring(1);
        if (Character.isUpperCase(firstLetter)) {
            return stringMissingFirst.substring(0, 1).toUpperCase() + stringMissingFirst.substring(1) + Character.toLowerCase(firstLetter);
        } else {
            return stringMissingFirst + Character.toLowerCase(firstLetter);
        }
    }
}
