package com.amd.telecom.assesment.exception;

public class AccessTokenConversionException extends Exception {
    public AccessTokenConversionException(String message) {
        super(message);
    }
}
