package com.amd.telecom.assesment.utils;

import org.junit.jupiter.api.Test;

class Base64EncoderDecoderTest {

    @Test
    public void encode() {
        // NWM1ZDVlMjhlNGIwYmFlNWY0YWNjZmVjOk1Ha05mcUd1ZDA=
        String expectedEncodedString = "NWM1ZDVlMjhlNGIwYmFlNWY0YWNjZmVjOk1Ha05mcUd1ZDA=";
        String toEncode = "5c5d5e28e4b0bae5f4accfec:MGkNfqGud0";
        String encode = Base64EncoderDecoder.encode(toEncode);
        if (encode.equals(expectedEncodedString)) {
            System.out.println("TEST PASSED!");
        } else {
            String msg = String.format("Expected %s, but got %s", expectedEncodedString, encode);
            System.out.println(msg);
        }
    }

}