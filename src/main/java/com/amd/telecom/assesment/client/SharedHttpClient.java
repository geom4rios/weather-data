package com.amd.telecom.assesment.client;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SharedHttpClient {

    /*
     The volatile modifier prevents subtle case where the compiler tries to optimize the
     code such that the object is accessed before it is finished being constructed.
    */
    private static volatile SharedHttpClient sharedHttpClient;
    private final HttpClient httpClient;

    private SharedHttpClient() {
        this.httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();
    }

    public static SharedHttpClient getInstance() {
        if (sharedHttpClient == null) {
            synchronized (SharedHttpClient.class) {
                if (sharedHttpClient == null) {
                   return new SharedHttpClient();
                }
            }
        }
        return sharedHttpClient;
    }

    public HttpResponse<String> send(HttpRequest request) throws IOException, InterruptedException {
        return this.httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }

}
