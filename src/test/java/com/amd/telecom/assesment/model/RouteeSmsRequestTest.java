package com.amd.telecom.assesment.model;


import org.junit.jupiter.api.Test;

class RouteeSmsRequestTest {

    @Test
    public void testToString() {

        RouteeSmsRequest routeeSmsRequest = new RouteeSmsRequest("A new game has been posted to the MindPuzzle. Check it out", "+306922222222");
        String toStringValue = routeeSmsRequest.toString();
        if (toStringValue.equals(expectedToStringValue)) {
            System.out.println("TEST PASSED!");
        } else {
            System.out.println("TEST FAILED");
            System.out.println("Expected: " + expectedToStringValue);
            System.out.println("Actual:   " + toStringValue);
        }

    }

    String expectedToStringValue = "{\"body\": \"A new game has been posted to the MindPuzzle. Check it out\",\"to\": \"+306922222222\",\"from\": \"amdTelecom\"}";
}