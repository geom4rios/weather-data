package com.amd.telecom.assesment.exception;

public class OpenWeatherDataConversionException extends Exception {
    public OpenWeatherDataConversionException(String message) {
        super(message);
    }
}
