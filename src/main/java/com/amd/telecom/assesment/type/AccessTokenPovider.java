package com.amd.telecom.assesment.type;

import java.util.Optional;

public interface AccessTokenPovider {

    Optional<String> generateAccessToken();

}
