package com.amd.telecom.assesment.smalExercises;

/*
If the sum is greater than 9 repeats the calculation of the sum of its digits until we get sum < 10.



Returns the final sum.



Examples:



10 -> 1 + 0 = 1 ... returns 1

38 -> 3 + 8 = 11 -> 1 + 1 = 2 ... returns 2

785 -> 7 + 8 + 5 = 20 -> 2 + 0 = 2  returns 2

99892 -> 9 + 9 + 8 + 9 + 2 = 37 -> 3 + 7 = 10 -> 1 + 0 = 1 returns 1
 */
public class DigitSum {

    public static void main(String[] args) {

        int[] test = new int[] { 10, 38, 785, 99892 };

        for (Integer num : test) {
            int result = digitSum(num);
            System.out.println(result);
        }
    }

    private static int digitSum(int number) {

        int nextNumber = number;
        do {
            nextNumber = findSum(nextNumber);
        } while (nextNumber > 9);

        return nextNumber;
    }
    
    private static int findSum(int num) {

        int sum = 0;
        
        while (num != 0) {
            sum += num % 10;
            num = num / 10;
        }
        
        return sum;
    }

}
